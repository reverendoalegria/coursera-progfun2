package quickcheck

import common._

import org.scalacheck._
import Arbitrary._
import Gen._
import Prop._

abstract class QuickCheckHeap extends Properties("Heap") with IntHeap {

  lazy val genHeap: Gen[H] = for {
    v <- arbitrary[Int]
    h <- oneOf(const(empty), genHeap)
  } yield insert(v, h)

  implicit lazy val arbHeap: Arbitrary[H] = Arbitrary(genHeap)

  property("gen1") = forAll { (h: H) =>
    val m = if (isEmpty(h)) 0 else findMin(h)
    findMin(insert(m, h)) == m
  }

  property("min1") = forAll { a: Int =>
    val h = insert(a, empty)
    findMin(h) == a
  }

  property("2onEmpty") = forAll { (a: Int, b: Int) =>
    val h = insert(a, insert(b, empty))
    val min = math.min(a, b)
    findMin(h) == min
  }

  property("deleteMin") = forAll { a: Int =>
    val h = insert(a, empty)
    val hr = deleteMin(h)
    isEmpty(hr)
  }

  property("meld") = forAll { (h1: H, h2: H) =>
    val h = meld(h1, h2)
    val min = findMin(h)
    min == findMin(h1) || min == findMin(h2)
  }

  def isSorted(hh: H, last: A): Boolean = {
    if (isEmpty(hh)) true
    else {
      val min = findMin(hh)
      if (min < last) false
      else isSorted(deleteMin(hh), min)
    }
  }

  property("sorted") = forAll { h: H =>
    isSorted(h, findMin(h))
  }

  property("insert2") = forAll { (a: Int, b: Int, c: Int, d: Int) =>
    val abmin = math.min(a, b)
    val cdmin = math.min(c, d)
    val h1 = insert(a, insert(b, empty))
    val h2 = insert(c, insert(d, empty))
    val hm = meld(h1, h2)
    val mmin = findMin(hm)
    findMin(h1) == abmin && findMin(h2) == cdmin && mmin == math.min(abmin, cdmin)
  }

  import math.min
  property("insert3") = forAll { (a: Int, b: Int, c: Int, d: Int) =>
    val m = min(a, min(b, min(c, d)))
    val h1 = insert(a, insert(b, insert(c, insert(d, empty))))

    findMin(h1) == m && isSorted(h1, findMin(h1))
  }

  property("meld2") = forAll { (a: Int, b: Int, c: Int, d: Int) =>
    val h1 = insert(a, insert(b, empty))
    val h2 = insert(c, insert(d, empty))
    val hm = meld(h1, h2)
    isSorted(hm, findMin(hm))
  }

  property("insert") = forAll { l: List[Int] =>
    def pop(h: H, l: List[Int]): Boolean = {
      if (isEmpty(h)) {
        l.isEmpty
      } else {
        l.nonEmpty && findMin(h) == l.head && pop(deleteMin(h), l.tail)
      }
    }
    val sl = l.sorted
    val h = l.foldLeft(empty)((he, a) => insert(a, he))
    pop(h, sl)
  }
}
