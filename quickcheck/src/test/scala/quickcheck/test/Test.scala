package quickcheck.test

import quickcheck.IntHeap

object Test extends IntHeap with Bogus4BinomialHeap with App {

  val h1 = insert(3, insert(2, insert(1, empty)))

  val m1 = findMin(h1)
  val hh = deleteMin(h1)

  val m2 = findMin(hh)
  val hhh = deleteMin(hh)

  val m3 = findMin(hhh)
  val hhhh = deleteMin(hhh)

}

