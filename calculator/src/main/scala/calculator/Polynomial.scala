package calculator

object Polynomial {
  def computeDelta(a: Signal[Double], b: Signal[Double],
      c: Signal[Double]): Signal[Double] = {
    Signal( math.pow(b(), 2.0) - 4 * a() * c())
  }

  def computeSolutions(a: Signal[Double], b: Signal[Double],
      c: Signal[Double], delta: Signal[Double]): Signal[Set[Double]] = {
    Signal {
      val d = delta()
      if (d < 0.0) Set[Double]()
      else {
        Set(
          (-b() + math.sqrt(delta())) / 2 * a(),
          (-b() - math.sqrt(delta())) / 2 * a()
        )
      }
    }
  }
}
